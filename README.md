
kiwiii-client
================

Kiwiii is a chemical data analysis and visualization platform based on HTTP server-client model.


Screen shots
--------------

![Table view](img/table-view.png)

![Network view](img/network-view.png)

![Control panel](img/control-panel.png)


Demo
--------------

A 1046 approved drug data extracted from DrugBank dataset (permitted to use under [Creative Common’s by-nc 4.0 License](https://creativecommons.org/licenses/by-nc/4.0/legalcode))

- [Table view](https://mojaie.github.io/kiwiii-client/datatable.html?location=ApprovedFiltered.json.gz)

- [Network view](https://mojaie.github.io/kiwiii-client/graph.html?location=ApprovedFiltered_GLS10.json.gz)


Keywords
--------------

Drug discovery, Cheminformatics, Data visualization


License
--------------

[MIT license](http://opensource.org/licenses/MIT)



Copyright
--------------

(C) 2014-2017 Seiji Matsuoka
